'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function triggerEvent(event) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    console.log('%cEvent emitted: ' + event, 'background:#000;color:chartreuse;padding:0 10px;', data);
    $_(window).trigger(event, [data]);
}

function injectScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0];
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
        if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {

            if (typeof callback === 'function') {
                done = true;
                callback();
            }

            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        }
    };

    head.appendChild(script);
}

function injectQueue() {
    var scripts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    var loaded = [];

    var triggerEventWhenAllLoaded = function triggerEventWhenAllLoaded() {
        if (loaded.length == scripts.length) {
            triggerEvent('allModulesLoaded', { modules: loaded });
            console.log('%c[' + window.ifcDevToolsContext.options.appName + '] ✔️ All modules loaded!', 'color: green;');
            if (typeof callback === 'function') {
                callback();
            }
        }
    };

    var _loop = function _loop(i) {
        var scriptUrl = scripts[i];
        if (_typeof(scripts[i]) === 'object') {
            scriptUrl = scripts[i].url;
        }
        injectScript(scriptUrl, function () {
            if (!!scripts[i].callback) {
                scripts[i].callback();
            }
            loaded.push(scriptUrl);
            triggerEventWhenAllLoaded();
        });
    };

    for (var i = 0; i < scripts.length; i++) {
        _loop(i);
    }
}

function showToast(texto) {
    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var timeToHide = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5 * 1000;

    var $toast = $_(window.ifcDevToolsContext.options.selectors.general.toast);
    var timeout = '';

    $_(document).on('click', window.ifcDevToolsContext.options.selectors.general.toast + ' .close', function () {
        $toast.fadeOut(300);
    });

    if ($toast.length == 0) {
        $_('body').append('\n            <div id="devtools-toast"><div class="close"></div><div class="content"></div></div>\n        ');
        $toast = $_(window.ifcDevToolsContext.options.selectors.general.toast);
    }
    if (!!type) {
        $toast.removeClass('good bad');
        $toast.addClass(type);
    } else {
        $toast.removeClass('good bad');
    }

    $toast.find('.content').html(texto);
    $toast.stop(0, 1).fadeIn(300);

    timeout = setTimeout(function () {
        $toast.stop(0, 1).fadeOut(300);
    }, timeToHide);
}

function getNotificationPermission() {
    return new Promise(function (resolve, reject) {
        Notification.requestPermission().then(function (status) {
            if (status == 'granted') {
                resolve();
            } else {
                reject(status);
            }
        });
    });
}

function getNotificationStatus() {
    if (!window.Notification) {
        return 'unsupported';
    }
    return window.Notification.permission;
}

function sendNotification(titulo, texto) {
    getNotificationPermission().then(function () {
        var n = new Notification(titulo, {
            body: texto
        });
    }).catch(function (status) {});
}