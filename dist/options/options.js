// chrome.options.set('Monaco Editor WordWrap');

chrome.options.opts = {
    title: 'MktPlace Get Data',
    about: 'Extensão desenvolvida pela equipe de Front. Iniciativa criada por Henrique Kuwai e Caroline Fernandes Veríssimo. Para mais detalhes, entre no repositório: <a href="https://gitlab.com/henriquekuwai/open-source/mktplace-getdata">https://gitlab.com/henriquekuwai/open-source/mktplace-getdata</a>',
    autoSave: true,
    saveDefaults: true,
};

// chrome.options.addTab('Teste', 'Descrição', [
//     {
//         type: 'checkbox',
//         name: 'option',
//         desc: 'Teste',
//         default: false,
//         singleline: true
//     }
// ]);