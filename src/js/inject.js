function injectScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0];
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function(){
      if ( !done && (!this.readyState
           || this.readyState == 'loaded'
           || this.readyState == 'complete') ) {

        if (typeof callback === 'function') {
            done = true;
            callback();
        }

        script.onload = script.onreadystatechange = null;
        head.removeChild(script);
      }
    };

    head.appendChild(script);
}

function injectLink(file, callback) {
    callback = typeof callback !== 'undefined' ? callback : '';

    var s = document.createElement("link");
    s.setAttribute('rel', 'stylesheet');
    s.setAttribute('href', file);
    document.head.appendChild(s);

    if (typeof callback === 'function') {
        s.onload = callback();
    }
}

injectLink('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

injectScript( chrome.extension.getURL('js/inject-with-window.js'), () => {
    var data = {
        absolutePath: chrome.runtime.getURL(''),
        urls: {
            jquery: chrome.runtime.getURL('js/vendors/jquery.min.js'),
            mustache: chrome.runtime.getURL('js/vendors/mustache.js')
        }
    };

    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent('initExtension', true, true, data);
    document.dispatchEvent(evt);
});