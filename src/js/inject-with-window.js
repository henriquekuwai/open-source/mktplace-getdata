function copyToClipboard(valor) {
	let tempSelector = 'tempClipboard';

	var element = document.createElement('textarea');
	element.setAttribute('id', tempSelector);
	element.value = valor;
	document.body.appendChild(element);
	// console.log(element);

	element.select();
	document.execCommand('copy');

	element.remove();

	console.log('Copiado: ' + valor);
}

function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
    document.execCommand("Copy");
}

function injectScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0];
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
      if ( !done && (!this.readyState
           || this.readyState == 'loaded'
           || this.readyState == 'complete') ) {

        if (typeof callback === 'function') {
            done = true;
            callback();
        }

        script.onload = script.onreadystatechange = null;
        head.removeChild(script);
      }
    };

    head.appendChild(script);
}
class mktPlaceGetData {
    constructor() {
        this.$window = $_(window);
        this.$document = $_(document);
        this.$body = $_('body');

        this.options = {
            contextFind: {
                magaluOrderEdit: '/Order/Edit',
                magaluOrderList: '/Order',
                walmartOrderEdit: '?seller_order_id=',
                correiosListagem: 'rastreamento/multResultado',
            },
            templates: {
                table: `
                    <div class="mg-action-area">
                        <a href="#" class="mg-btn copyTable">Copiar Linha(s)</a>
                    </div>
                    <table class="mg-table" width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                            {{#rows}}
                                <tr>{{{.}}}</tr>
                            {{/rows}}
                        </tbody>
                    </table>
                `,
                row: `
                {{#cells}}
                    <td>{{{.}}}</td>
                {{/cells}}
                `
            },
            selectors: {
                tempData: '.mg-tempdata',
                lightbox: '.mg-lightbox',
                lightboxOverlay: '.mg-lightbox-overlay',
                lightboxClose: '.mg-lightbox-close',
                lightboxContent: '.mg-lightbox-content',
                ordersCheckbox: 'input[name^=getOrders]',
                getAllRowsData: '.getAllRowsData',
                getAllPageData: '.getAllPageData',
                getAllPostedDataBtn: '.getAllPostedData',
            },
            api: {
                magaluDetail: 'https://marketplace.integracommerce.com.br/Order/Edit?id=',
                walmartDetail: 'https://stargate.wmxp.com.br/orders/74900183?seller_order_id='
            },
            productSkus: {
                'Smartphone Qbex Flix 8GB 4G LTE Dual Chip Desbloqueado Cinza': {
                    sku: '7898631690997',
                    id: '62430984'
                },
                'Smartphone Qbex Flix 8GB 4G LTE Dual Chip Desbloqueado Dourado': {
                    sku: '7898631691000',
                    id: '62431742'
                },
                'Tablet Qbex TX240 7.85" 8GB Dual Core A23 Android 4.4 Prata': {
                    sku: '7898631691222',
                    id: '62431800'
                }
            }
        };

        this.location = '';
        this.context = '';

        window.mgCurrentRequest = 0;
        window.mgRequestQueue = [];
        window.mgRequestData = [];

        this.isRetrieving = false;

        this.identifyContext();

        this.emitters();
        this.listeners();
        this.initFunctions();
    }
    getOrderId(valor) {
        let split1 = valor.split('id=');
        let split2 = split1[split1.length-1].split('&');

        return split2[0];
    }
    identifyContext() {
        if (window.location.href.indexOf('marketplace.integracommerce.com.br') > -1) {
            this.location = 'magalu';

            if (window.location.href.indexOf(this.options.contextFind.magaluOrderEdit) > -1) {
                this.context = 'magaluOrderEdit';
            } else if (window.location.href.indexOf(this.options.contextFind.magaluOrderList) > -1) {
                this.context = 'magaluOrderList';
            }
        }
        if (window.location.href.indexOf('stargate.wmxp.com.br') > -1) {
            this.location = 'walmart';

            if (window.location.href.indexOf(this.options.contextFind.walmartOrderEdit) > -1) {
                this.context = 'walmartOrderEdit';
            }
        }
        if (window.location.href.indexOf('correios.com.br') > -1) {
            this.location = 'correios';

            if (window.location.href.indexOf(this.options.contextFind.correiosListagem) > -1) {
                this.context = 'correiosListagem';
            }
        }
    }
    initFunctions() {
        this.$body.append(`
        <div class="mg-lightbox-overlay"></div>
        <div class="mg-lightbox">
            <div class="mg-lightbox-close">fechar</div>
            <div class="mg-lightbox-content"></div>
        </div>
        <div class="mg-tempdata"></div>
        `);

        if (this.location == 'magalu') {

            if (this.context == 'magaluOrderEdit') {
                // tela de edição
            }
            if (this.context == 'magaluOrderList') {
                this.$body.append(`
                    <div class="mg-wrapper">
                        <a href="#" class="mg-btn getAllRowsData">Extrair dados marcados</a>
                        <a href="#" class="mg-btn getAllPageData">Extrair todos os pedidos</a>
                    </div>
                `);

                $_('#tableOrders tbody > tr').each((i, e) => {
                    let $td = $_(e).find('td:eq(1)');
                    let link = $td.find('a').attr('href');
                    let orderId = this.getOrderId(link);

                    $td.append(`
                        <a href="${this.options.api.magaluDetail + orderId}" class="mg-block getRowData" data-order-id="${orderId}">Extrair dados</a>
                        <label class="mg-checkbox-label"><input type="checkbox" name="getOrders[]" value="${orderId}" /> Marcar</label>
                    `)
                });
            }
        }
        if (this.location == 'correios') {

            if (this.context == 'correiosListagem') {
                this.$body.append(`
                    <div class="mg-wrapper">
                        <a href="#" class="mg-btn getAllPostedData">Extrair datas de postagem</a>
                    </div>
                `);

                let n = 0;

                $_('.codSro').each((i, e) => {
                    let codigo = $_(e).text().trim();
                    $_(e).attr('data-codigo', codigo);

                    let $statusTd = $_(e).closest('tr').find('td:eq(1)');
                    $statusTd.addClass('sroStatus');

                    let $lastChangeTd = $_(e).closest('tr').find('td:eq(2)');
                    $lastChangeTd.addClass('sroLastChange');
                });

                let processQueue = () => {
                    let $nextItem = $_('.codSro:not(.retrieved):not(.retrieving):eq(0)');

                    if ($nextItem.length > 0) {
                        if (!this.isRetrieving) {
                            let codigo = $nextItem.text().trim();
                            let $el = $_('[data-codigo=' + codigo + ']');

                            this.isRetrieving = true;
                            $el.addClass('retrieving');
                            $_(this.options.selectors.tempData).load('http://www2.correios.com.br/sistemas/rastreamento/resultado.cfm .listEvent', { objetos: codigo }, () => {
                                let dataPostado = $_(this.options.selectors.tempData).find('tr:last-child .sroDtEvent').html().split('<br>')[0];
                                $el.closest('td').append(`<div class="postadoEm">Postado em: ${dataPostado}</div>`);
                                $el.attr('data-postado', dataPostado);

                                this.isRetrieving = false;
                                $_(this.options.selectors.tempData).html('');
                                $el.removeClass('retrieving').addClass('retrieved');
                                processQueue();
                            });
                        }
                    }
                };

                processQueue();
            }
        }
    }
    getDataAsTable() {
        let $codSroData = $('.codSro.retrieved');
        let $statusSro = $('.sroStatus');
        let $lastChangeSro = $('.sroLastChange');
        let rows = [];
        
        for (let i = 0; i < $codSroData.length; i++) {
            let cells = [];
            
            cells.push($codSroData.eq(i).attr('data-postado'));

            rows.push(
                Mustache.render(this.options.templates.row, { cells: cells })
            );
        }

        return rows;
    }
    emitters() {

    }
    showData(rows) {
        if (!!rows) {
            let htmlFinal = Mustache.render(this.options.templates.table, { rows: rows });

            $_(this.options.selectors.lightboxContent).html(htmlFinal);
            $_(this.options.selectors.lightboxOverlay).addClass('visible');
            $_(this.options.selectors.lightbox).addClass('visible');
        } else {
            alert('Nenhum dado retornado para exibição.');
        }
    }
    htmlToRows(data) {
        $_(this.options.selectors.tempData).html(data);

        let getSiblingHtml = ($obj) => {
            let valor = $obj.closest('tr').find('td:last-child').html();

            if (!!valor) valor = valor.replace(/(R\$)/gi, '').trim();

            return valor;
        };
        let $obj = $_(this.options.selectors.tempData);
        let qtdProdutos = $obj.find('label[for=orderProduct_Quantity]').length;
        let rows = [];

        for (let i = 0; i < qtdProdutos; i++) {
            let cells = [];

            let qtd = getSiblingHtml($obj.find('label[for=orderProduct_Quantity]:eq(' + i + ')'));
            let unitPrice = getSiblingHtml($obj.find('label[for=orderProduct_Price]:eq(' + i + ')'));
            let desconto = (getSiblingHtml($obj.find('label[for=orderProduct_Discount]:eq(' + i + ')')) != '')
            ? getSiblingHtml($obj.find('label[for=orderProduct_Discount]:eq(' + i + ')'))
            : 0;
            let unitPriceWithDesc = parseFloat(unitPrice) - parseFloat(desconto);
            let cartTotalWithoutFreight = +qtd * parseFloat(unitPriceWithDesc);

            cells.push(getSiblingHtml($obj.find('label[for=PurchaseDate]')));
            cells.push(getSiblingHtml($obj.find('label[for=IdOrder]')));
            cells.push('Envio pendente');
            cells.push('Magazine Luiza');
            cells.push(
                (getSiblingHtml($obj.find('label[for=ApprovedDate]'))).split(' ')[0]
            );
            if (!!this.options.productSkus[getSiblingHtml($obj.find('label[for=orderProduct_Name]:eq(' + i + ')'))]) {
                cells.push(
                    this.options.productSkus[getSiblingHtml($obj.find('label[for=orderProduct_Name]:eq(' + i + ')'))].sku
                );
            } else {
                cells.push('SKU?');
            }
            cells.push(getSiblingHtml($obj.find('label[for=orderProduct_Name]:eq(' + i + ')')));
            if (!!this.options.productSkus[getSiblingHtml($obj.find('label[for=orderProduct_Name]:eq(' + i + ')'))]) {
                cells.push(
                    this.options.productSkus[getSiblingHtml($obj.find('label[for=orderProduct_Name]:eq(' + i + ')'))].id
                );
            } else {
                cells.push('PID?');
            }
            cells.push(qtd);
            cells.push(unitPrice);
            cells.push(unitPriceWithDesc);
            cells.push(getSiblingHtml($obj.find('label[for=CustomerPfName]')));
            cells.push(getSiblingHtml($obj.find('label[for=CustomerMail]')));
            cells.push(getSiblingHtml($obj.find('label[for=CustomerPfCpf]')));
            cells.push(''); // sexo
            cells.push(getSiblingHtml($obj.find('label[for=TelephoneMainNumber]')));
            cells.push(''); // tel comercial
            cells.push(getSiblingHtml($obj.find('label[for=TelephoneSecundaryNumber]')));
            cells.push(
                getSiblingHtml($obj.find('label[for=DeliveryAddressStreet]')) +
                (!!getSiblingHtml($obj.find('label[for=DeliveryAddressAdditionalInfo]')) ? ' ' + getSiblingHtml($obj.find('label[for=DeliveryAddressAdditionalInfo]')) : '')
            );
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressNeighborhood]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressZipcode]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressCity]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressState]')));
            cells.push(
                getSiblingHtml($obj.find('label[for=DeliveryAddressStreet]')) +
                (!!getSiblingHtml($obj.find('label[for=DeliveryAddressAdditionalInfo]')) ? ' ' + getSiblingHtml($obj.find('label[for=DeliveryAddressAdditionalInfo]')) : '')
            );
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressNeighborhood]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressZipcode]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressCity]')));
            cells.push(getSiblingHtml($obj.find('label[for=DeliveryAddressState]')));
            cells.push(
                $obj.find('#payment_tab .form-control:eq(0)').val()
            );
            cells.push(cartTotalWithoutFreight);
            cells.push(getSiblingHtml($obj.find('label[for=TotalFreight]')));
            cells.push(
                (getSiblingHtml($obj.find('label[for=TotalDiscount]')) != '') ?
                getSiblingHtml($obj.find('label[for=TotalDiscount]')) :
                '0'
            );
            cells.push(getSiblingHtml($obj.find('label[for=TotalAmount]')));
            cells.push(''); // cupom
            cells.push(''); // promo
            cells.push('PAC');
            cells.push(getSiblingHtml($obj.find('label[for=orderProduct_SkuId]')));
            cells.push(''); // entrega amazon
            cells.push(''); // entrega walmart
            cells.push(getSiblingHtml($obj.find('label[for=EstimatedDeliveryDate]')));
            cells.push(''); // entrega cnova

            rows.push(
                Mustache.render(this.options.templates.row, { cells: cells })
            );
        }

        return rows;
    }
    processRequests(e) {
        if (window.mgCurrentRequest < window.mgRequestQueue.length) {
            if (window.mgCurrentRequest == 0) {
                window.mgBtnOldText = $_(e.currentTarget).html();
            }
            $_(e.currentTarget).addClass('loading');
            $_(e.currentTarget).html('Carregando... <span id="mg-queue-info">' + (+window.mgCurrentRequest + 1) + '</span>/<span id="mg-queue-total">' + window.mgRequestQueue.length + '</span>');
            if (!!window.mgRequestQueue[window.mgCurrentRequest]) {
                $_.ajax({
                    url: this.options.api.magaluDetail + window.mgRequestQueue[window.mgCurrentRequest],
                    dataType: "html"
                })
                .done((data) => {
                    if (!!data) {
                        console.log('Request para ' + window.mgRequestQueue[window.mgCurrentRequest] + ' realizado com sucesso!');
                        data = $_.parseHTML(data);
                        window.mgRequestData.push(this.htmlToRows(data));

                        window.mgCurrentRequest++;
                        this.processRequests(e);
                    }
                })
                .fail(() => {
                    console.error('Erro no request para o order ' + window.mgRequestQueue[window.mgCurrentRequest]);
                    window.mgCurrentRequest++;
                });
            } else {
                window.mgCurrentRequest++;
            }
        } else {
            $_(e.currentTarget).removeClass('loading');
            $_(e.currentTarget).html(window.mgBtnOldText);
            window.mgBtnOldText = '';
            this.showData(window.mgRequestData);
            window.mgCurrentRequest = 0;
            window.mgRequestQueue = [];
            window.mgRequestData = [];
        }
    }
    listeners() {
        this.$document.on('change', this.options.selectors.ordersCheckbox, () => {
            let orders = [];
            $_(this.options.selectors.ordersCheckbox + ':checked').each(
                (i,e) => orders.push($_(e).val())
            );

            if (orders.length == 0) {
                $_(this.options.selectors.getAllRowsData).removeClass('visible');
                return false;
            }
            $_(this.options.selectors.getAllRowsData).addClass('visible');
        });

        this.$document.on('click', this.options.selectors.getAllRowsData, (e) => {
            e.preventDefault();
            let orders = [];
            $_(this.options.selectors.ordersCheckbox + ':checked').each(
                (i,e) => orders.push($_(e).val())
            );

            if (!orders.length) {
                alert('Nenhum pedido marcado!');
                return false;
            }

            window.mgCurrentRequest = 0;
            window.mgRequestQueue = orders;

            this.processRequests(e);
        });

        this.$document.on('click', this.options.selectors.getAllPageData, (e) => {
            e.preventDefault();
            let orders = [];
            $_(this.options.selectors.ordersCheckbox).each(
                (i,e) => orders.push($_(e).val())
            );

            if (!orders.length) {
                alert('Nenhum pedido marcado!');
                return false;
            }

            window.mgCurrentRequest = 0;
            window.mgRequestQueue = orders;

            this.processRequests(e);
        });

        this.$document.on('click', this.options.selectors.getAllPostedDataBtn, (e) => {
            e.preventDefault();
            this.showData(this.getDataAsTable());
        });

        this.$document.on('click', '.getRowData', (e) => {
            e.preventDefault();
            let orderId = $_(e.currentTarget).attr('data-order-id');

            window.mgCurrentRequest = 0;
            window.mgRequestQueue = [orderId];

            this.processRequests(e);
        });

        this.$document.on('click', '.copyTable', (e) => {
            e.preventDefault();
            let $table = $_('.mg-lightbox-content .mg-table');

            selectElementContents($table.get()[0]);
        });

        this.$document.on('click', this.options.selectors.lightboxClose + ', ' + this.options.selectors.lightboxOverlay, () => {
            $_(this.options.selectors.lightboxOverlay).removeClass('visible');
            $_(this.options.selectors.lightbox).removeClass('visible');
        });

        this.$document.on('keyup', (e) => {
            if (e.keyCode == 27) {
                $_(this.options.selectors.lightboxClose).click();
            }
        });
    }
}

document.addEventListener('initExtension', (event) => {

    var data = event.detail;

    injectScript( data.urls.mustache, () => {

        /* ------------ Injeção do jQuery */
        injectScript( data.urls.jquery, () => {
            window.$_ = $.noConflict(true);
            console.log('%c[mktplace-getdata] ⚠️ jQuery ' + $_.fn.jquery + ' loaded!', 'color:#edc91a;');

            window.mktplace = new mktPlaceGetData();
        });

    });

});