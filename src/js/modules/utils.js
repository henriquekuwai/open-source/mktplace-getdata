function triggerEvent(event, data = {}) {
    console.log('%cEvent emitted: ' + event, 'background:#000;color:chartreuse;padding:0 10px;', data);
    $_(window).trigger(event, [data]);
}

function injectScript(url, callback) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0];
    var done = false;

    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
      if ( !done && (!this.readyState
           || this.readyState == 'loaded'
           || this.readyState == 'complete') ) {

        if (typeof callback === 'function') {
            done = true;
            callback();
        }

        script.onload = script.onreadystatechange = null;
        head.removeChild(script);
      }
    };

    head.appendChild(script);
}

function injectQueue(scripts = [], callback = '') {
    let loaded = [];

    let triggerEventWhenAllLoaded = () => {
        if (loaded.length == scripts.length) {
            triggerEvent('allModulesLoaded', { modules: loaded });
            console.log('%c[' + window.ifcDevToolsContext.options.appName + '] ✔️ All modules loaded!', 'color: green;');
            if (typeof callback === 'function') {
                callback();
            }
        }
    };

    for (let i = 0; i < scripts.length; i++) {
        let scriptUrl = scripts[i];
        if (typeof scripts[i] === 'object') {
            scriptUrl = scripts[i].url;
        }
        injectScript(scriptUrl, () => {
            if (!!scripts[i].callback) {
                scripts[i].callback();
            }
            loaded.push(scriptUrl);
            triggerEventWhenAllLoaded();
        });
    }
}

function showToast(texto, type = '', timeToHide = 5*1000) {
    let $toast = $_(window.ifcDevToolsContext.options.selectors.general.toast);
    let timeout = '';

    $_(document).on('click', window.ifcDevToolsContext.options.selectors.general.toast + ' .close', () => {
        $toast.fadeOut(300);
    });

    if ($toast.length == 0) {
        $_('body').append(`
            <div id="devtools-toast"><div class="close"></div><div class="content"></div></div>
        `);
        $toast = $_(window.ifcDevToolsContext.options.selectors.general.toast);
    }
    if (!!type) {
        $toast.removeClass('good bad');
        $toast.addClass(type);
    } else {
        $toast.removeClass('good bad');
    }

    $toast.find('.content').html(texto);
    $toast.stop(0,1).fadeIn(300);

    timeout = setTimeout(() => {
        $toast.stop(0,1).fadeOut(300);
    }, timeToHide);

}

function getNotificationPermission() {
    return new Promise((resolve, reject) => {
        Notification.requestPermission().then(status => {
            if (status == 'granted') {
                resolve();
            }else{
                reject(status);
            }
        });
    });
}

function getNotificationStatus() {
    if (!window.Notification) {
        return 'unsupported';
    }
    return window.Notification.permission;
}

function sendNotification(titulo, texto) {
    getNotificationPermission()
    .then(function () {
        var n = new Notification(titulo, {
            body: texto
        });
    }).catch(function (status) { });
}