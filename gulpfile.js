var gulp = require('gulp');
var pump = require('pump');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var gulpImports = require('gulp-imports');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var argv = require('yargs').argv;
var watchSass = require("gulp-watch-sass")
var isDeploy = (argv.deploy === undefined) ? false : true;

var dir = {
    css: {
        src: [
            'src/scss/**/*'
        ],
        dist: 'dist/css'
    },
    js: {
        src: [
            'src/js/*.js'
        ],
        dist: 'dist/js'
    },
    jsModules: {
        src: [
            'src/js/modules/*.js'
        ],
        dist: 'dist/js/modules'
    },
    move: {
        src: [
            'src/js/vendors/*.js',
        ],
        dist: 'dist/js/vendors'
    }
};

gulp.task('css', function (cb) {
    if (!!isDeploy) {
        pump([
            gulp.src(dir.css.src),
            sourcemaps.init(),
            sass().on('error', sass.logError),
            cleanCSS(),
            gulp.dest(dir.css.dist)
          ],
          cb
        );
    } else {
        pump([
            gulp.src(dir.css.src),
            sourcemaps.init(),
            sass().on('error', sass.logError),
            gulp.dest(dir.css.dist)
        ],
        cb
        );
    }
});

gulp.task('js', function(cb){
    if (!!isDeploy) {
        pump([
            gulp.src(dir.js.src),
            gulpImports(),
            babel({
                presets: ['env']
            }),
            uglify({ mangle: { reserved: ['jQuery', '$', '$_', 'Store'] } }),
            gulp.dest(dir.js.dist)
          ],
          cb
        );
    } else {
        pump([
            gulp.src(dir.js.src),
            gulpImports(),
            babel({
                presets: ['env']
            }),
            gulp.dest(dir.js.dist)
        ],
        cb
        );
    }
});

gulp.task('jsModules', function(cb){
    pump([
        gulp.src(dir.jsModules.src),
        gulpImports(),
        babel({
            presets: ['env']
        }),
        gulp.dest(dir.jsModules.dist)
    ],
    cb
    );
});

gulp.task('move', function(cb){
    pump([
        gulp.src(dir.move.src),
        gulp.dest(dir.move.dist)
    ],
    cb
    );
});

gulp.task('watch', ['css', 'js', 'move', 'jsModules'], function (cb) {
    gulp.watch(dir.js.src, ['js']);
    gulp.watch(dir.jsModules.src, ['jsModules']);
    gulp.watch(dir.move.src, ['move']);
    gulp.watch(dir.css.src, ['css']);
});

gulp.task('default', ['css', 'js', 'move', 'jsModules']);