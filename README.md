# Marketplace - Get Data

Extensão para capturar dados dos parceiros de maneira mais fácil.

A extensão captura os dados e já retorna em formato de tabela, para copiar e colar no Excel.

-------------

## Como usar

* A extensão já identifica o contexto do parceiro, ao logar, e adiciona as opções na tela.
* No caso de Magazine Luiza, você pode extrair os dados de pedido por pedido (a partir da listagem), ou marcar mais de um e extrair de uma vez.
* Um modal se abrirá com as tabelas formatadas.
* Clique no botão "Copiar linha(s)" para copiar e colar no Excel.
* É recomendável utilizar o "Colar especial...", marcando como "Texto".
* Confira os dados; pode estar faltando dados como sexo, por exemplo, pois não vêm nos dados do parceiro.

-------------

## Parceiros integrados

* Magazine Luiza (Integra Commerce)

## Features

* Correios: Listagem de status de rastreios traz já a data de postagem do objeto, com opção para cópia em formato de tabela (para Excel)




